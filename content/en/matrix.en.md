+++
title = "[Matrix] Instructions"
hideReadMore = true
translationKey = "matrix"
slug = "matrix"
+++

# What is [Matrix]?
[matrix] is a federated chat protocol like IRC or XMPP (formerly known as Jabber). [element](https://element.io/get-started) is the most popular client for [matrix]. You can also use other clients, but some things may not work as they do for most (e.g. end-to-end encryption). [element](https://element.io/get-started) is available as a browser app, for desktop (Linux, Mac, or Windows), and for Android and iPhone.

We have our own server, but you don't need to have an account on our server to join our space.

# What is a space?
To avoid having all of our communication take place in a single room, we have a F.U.C.K. Space in which different rooms on different topics are bundled. (This is like workspaces in Slack.) When you come into our space, you can see--and also join, if you'd like--all the rooms.

# How can I sign up?
If you don't have an account on our server yet, you can receive a login token from us. You can use the token to register for an account. The screenshots below are from the browser app.

# What do I do with the token?
You have received a link from us with which you can register. On the linked page, the token is already entered, or you can enter it into the token field. Choose a username and use a secure password.


![fuck-the.systems-registration](/element/00_register.png)

# Log in
You will then come to the login screen; you can also perform the following steps in your favorite app.


![fuck-the.systems Signin](/element/01_signin.png)

# The right home server
In the registration form, you must ensure that the correct home server is entered. This is especially important if you are using an app in which the correct home server is not automatically set to "fuck-the.systems". Most of the time it says "matrix.org", the default server of [matrix].


![fuck-the.systems signup form](/element/02_signin_server.png)

# Invitation to the Space
Once you have logged in, you still need the invitation to the space. You can get an invitation from anyone who is already in our space. If in doubt, you can also write to us in our public room. Or you can simply write via the same communication channel that you used to receive the token. As soon as you receive an invitation, a field with our logo and a red exclamation mark will appear on the left-hand side. Click on it and you should be able to accept the invitation. If that doesn't work right away, it's best to ask one of us so that we can work it out together (sometimes it's a bit slow or doesn't work the first time. The space feature is not that old and sometimes still a bit unreliable.)


![Fuck Space Invitation](/element/04_invite_fuck.png)

# The room overview
The first thing you should see is the room overview of our space. You can connect all these rooms, for the beginning "main" and "welcome" are particularly exciting. "main" is our central channel for sharing information, chatting, announcing events, etc. The other rooms are on specific topics.


![Room overview](/element/06_room_overview.png)


You can always return to the room overview by clicking on the space logo on the left.


![Back to room overview](/element/09_back_to_overview.png)


# Hooray you did it! Happy chatting! <3
Everything that comes now is optional or may be relevant for you later.

# Secure backup?
Pretty soon a dialog will appear on the top left side asking you if you want to make a secure backup. You should save this so that you can also read encrypted messages in the future if you have logged out or want to use another device. It is best to save the code in a password manager or another place where you can find it again, but preferably in a way that is not too easy to find (for anyone who is not you).


![Secure Backup](/element/07_secure_backup.png)


Element encrypts communication end-to-end (if enabled for the room), meaning you always need a valid key to read the messages in an encrypted room. If you log in from a new device or with a different browser, the old messages cannot be read either. When logging in from a device, you must first prove that you are authorized to decrypt the messages. A dialog will appear again at the top left and then you have two options.


![Verify device](/element/10_verify_device.png)


Verify with Security Key means you enter the code you had saved. This always works, regardless of whether you are still logged in to another session.

Alternatively, you can mutually verify your devices/sessions. You must have access to both sessions at the same time, for example mobile and desktop. If you start a verification with one device, a verification request appears on the other device.


![Verification request](/element/13_other_device.png)


You accept the request and you compare whether the same emojis are displayed on both devices. If so, all you have to do is confirm and from now on both devices should be able to read the same encrypted messages. Incidentally, verification with other users also works according to the same principle.

![Compare Emojis](/element/15_emoji_compare.png)


# Problems setting up?
We have a [Public Support Channel](https://element.fuck-the.systems/#/room/#support:fuck-the.systems) for quick help and troubleshooting if you're already on Matrix. Otherwise you can ping us on our other channels if something doesn't work.