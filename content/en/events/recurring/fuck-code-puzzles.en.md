+++
title = "F.U.C.K. Code Puzzles"
summary = "Every Thursday from 19:00 - 20:00"
readmore = "More info..."
weight = 40
translationKey = "code-puzzles"
slug = "fuck-code-puzzles"
+++

# When?
Every Thursday from 19:00 - 20:00 (before F.U.C.K. Thursday or F.U.C.K. Donnerstag)

# Where?
On Jitsi (link is in the [F.U.C.K.-Matrix](https://matrix.to/#/#public:fuck-the.systems), via [Email](mailto:hello@fuck-the.systems) or [Mastodon DM](https://chaos.social/@fuckthesystems)).

# What?
Before the Thursday meeting we meet and work on a coding puzzle together. You are welcome to bring puzzles, otherwise it will probably be a coding puzzle from the internet.
If we are more than 3 people we will split into smaller groups. The language with which we solve results from those present in the group.
Even if you don't even know how to start, you're very welcome and you're always welcome to tinker with us.

# Language
German or English depending on who is in the group.

# Meeting Guide
If you want to visit one of our meetings for the first time, please take a look at our [meeting guide](/en/meetings/).