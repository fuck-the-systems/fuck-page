+++
title = "F.U.C.K. Tuesday - Do Things"
summary = "Every 1st and 3rd Tuesday of the month from 19:00"
readmore = "More info..."
weight = 30
translationKey = "tuesday"
slug = "fuck-tuesday"
+++

# When?
On the 1st and 3rd Tuesday of the month at 7:00 p.m

# Where?
On Jitsi (link is in the [F.U.C.K.-Matrix](https://matrix.to/#/#public:fuck-the.systems), via [Email](mailto:hello@fuck-the.systems), or [Mastodon DM](https://chaos.social/@fuckthesystems))

# What?
The Tuesday meetings are intended for working together (or alone) on projects. At 7:00 p.m. there will be a short call in which we will tell each other what we are working on. Then we split up into smaller groups or people leave the Jitsi to work on their project in peace.
You can bring ready-made project ideas with you. If you would like to start coding, but don't really know how and where, you can also come by and get tips and links to tutorials.

# Language
German or English depending on who is in the group.

# Meeting Guide
If you want to visit one of our meetings for the first time, please take a look at our [meeting guide](/en/meetings/).