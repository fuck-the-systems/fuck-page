+++
title = "F.U.C.K. Thursday"
summary = "Every 2nd and 4th Thursday of the month from 20:00 in English"
readmore = "More info..."
weight = 10
translationKey = "thursday"
slug = "fuck-thursday"
+++

# When?
On the 2nd and 4th Thursday of the month (alternating with [F.U.C.K. Donnerstag](/events/recurring/fuck-donnerstag/))
8:00 p.m. until no one is there

# Where?
On Jitsi (link is in the [F.U.C.K.-Matrix](https://matrix.to/#/#public:fuck-the.systems), or via [Email](mailto:hello@fuck-the.systems), or [Mastodon DM](https://chaos.social/@fuckthesystems))

# What?
The Thursday meeting is the best place to just drop by and to get to know F.U.C.K. There are rounds of introductions and brief explanations of the F.U.C.K. Network. It's just a nice round of chatting where the topics vary from week to week depending on what people want to chat about. There is also the possibility to form smaller groups if there are several topics or if the group is too large.


# Language: English

# Meeting Guide
If you want to visit one of our meetings for the first time, please take a look at our [meeting guide](/en/meetings/).