+++
title = "Feminism and Computer Stuff (Kram)"
date="2023-06-09"
cover = "logo-fuck-final.png"
translationKey = "home"
slug = "home"
+++

# Welcome to our online hackspace

We are women, lesbian, inter, non-binary, trans and agender people and we like computer stuff. If that applies to you too, you've come to the right place!


# Events
All dates can be found under [events](/en/events/)


# Contact

We have {{< rawhtml >}}<a rel="me" href="https://chaos.social/@fuckthesystems">Mastodon</a >{{< /rawhtml >}}

our public Matrix [Channel](https://matrix.to/#/#public:fuck-the.systems)

Write us an [Email](mailto:hello@fuck-the.systems)

# Projects

We operate our own infrastructure: currently a Matrix server, a Jitsi server and etherpads
