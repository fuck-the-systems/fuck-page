+++
title = "Guide for our Meetings"
hideReadMore = true
translationKey = "meetings"
slug = "meetings"
+++

# How to: Meetings with F.U.C.K.

## Before you enter:

 * use **headphones** to avoid echo
 * make sure you have a working headset, you can check if your **microphone is working** in the jitsi lobby
 * make sure there are no loud **background noises** (eg. take your cookies out of the rustling plastic packaging)
 * if you can: activate noise reduction on your headset or pc (e.g. noisetorch)
 * you can ask us to help with **testing** your setup in a separate meeting
 

 ## For first timers:
 
 * if there is a new person in the meeting there will be an **introduction round** where everyone can say a few words about themselves
 * if you join for the first time, ask **as many questions about f.u.c.k. as you want**, we'll prioritize those
 * you are now eligible for an **invite to our matrix space** where we talk about all kinds of stuff and coordinate meetings 
 

 ## In the meeting:
 
 * please be mindful of the **space you're taking** up in the discussion (how much you talk in relation to others)
 * make space for people who are more quiet
 * don't interrupt people
 * if you feel like you can't get a word in you can use the "raise hand" button
 * be mindful of topics that might be **emotionally sensitive** for people (eg. drugs, violence, mental health, climate doom, ...) **when in doubt, ask** if it's okay for everyone to talk about it
 * you can always (ask to) open up a second room if you want to talk about a sensitive topic or a topic that's only interesting to parts of the group (eg. niche tech talk)
 * you can always (ask to) **open up a second room** if you feel like there are too many people in the room (usually we do it when we exceed 6 people)
 * obviously: no discrimination, no screenshots, no condescendig remarks, no feigned surprise etc.
 * ask if it's okay for everyone if you're going to drink alcohol (ask again when new people come)
 * don't be super drunk or super stoned
 * if there is something bothering you, for example a background noise, flashing lights etc. chances are, that you're not the only one bothered by it and it's totally okay to **ask people if they can make it stop**
 * **take care of each other**. If you get the impression that someone isn't feeling well at the moment, ask them if they are OK.
 * If you notice a situation that might be uncomfortable for another person or notice any form of discrimination/harassment, or if a situation just seems a bit off to you, you are encouraged to step in and not to just let it happen. What might just seem a bit off to you could be worse for another person.
