+++
title = "F.U.C.K. FAQ"
readmore = "More info..."
translationKey = "faq"
slug = "faq"
+++

# Who is F.U.C.K. for?
We are women, lesbian, inter, non-binary, trans and agender people and we like computer stuff. If that applies to you too, you've come to the right place!


# May I come if I don't know how to program yet?
YES!!!

# Do I need any prior knowledge to come and see you?
No! You don't have to be able to program or run a Linux system or be familiar with the command line. We want to create a space together where you can learn it if you want.


# Do you have a Code of Conduct (CoC)?
Yes! Our Code of Conduct is [here](/en/coc/). If you have suggestions, you can open an issue in our [gitlab repo](https://gitlab.com/fuck-the-systems/code-of-conduct/) or contact us directly.

# Do I have to have Zoom or Google Meet or something else installed for an online meeting?
No. The meetings take place via Jitsi.
- on the computer you can simply open the link to the meeting in the browser without installing anything (it may be that not all browsers work equally well and if you have difficulties maybe try a different browser)
- on the phone:
  on your phone you need the Jitsi app to be able to connect
  [fdroid](https://f-droid.org/de/packages/org.jitsi.meet/),
  [Android Store](https://play.google.com/store/apps/details?id=org.jitsi.meet&gl=DE)
  [Apple Store](https://apps.apple.com/de/app/jitsi-meet/id1165103905)


# What happens during the meetings?
Look at the [dates](/events/) and click on more information. There you will find a description of the dates.
We have a [guide for our meetings](/en/meetings/) that you can check out.


# How do I get the link to a meeting?
The link for the events is available in [fuck-matrix](https://matrix.to/#/#public:fuck-the.systems), via [email](mailto:hello@fuck-the.systems), or [Mastodon DM](https://chaos.social/@fuckthesystems)


# I'm already there and would like to bring one person with me
Feel free to forward the link to individuals, please do not post the links to the events publicly anywhere.


# I would like to take part, but I don't like video calls
There is also a Matrix Space, where you can make video calls to F.U.C.K. can join.
Just ping us via [fuck-matrix](https://matrix.to/#/#public:fuck-the.systems), via [email](mailto:hello@fuck-the.systems), [Twitter DM] (https://twitter.com/_fuckthesystems) or [Mastodon DM](https://chaos.social/@fuckthesystems)


# How do I get into the Matrix Space?
You can be invited to the Matrix Space if you have been to one of our online meetings or if you know someone who is already in. If you already have an account, you can simply tell us your account name and we will send you an invitation.
If you don't have one yet, there are instructions [here](/en/matrix/) on how to register.


# Can I get the F.U.C.K. Use infrastructure for other feminist projects?
Yes, you are welcome to use our Matrix, Jitsi or Etherpads.
