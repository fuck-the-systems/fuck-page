+++
title = "Guide für unsere Meetings"
hideReadMore = true
translationKey = "meetings"
slug = "meetings"
+++

# Anleitung: Treffen mit F.U.C.K.

## Bevor du reinkommst:

 * Benutze **Kopfhörer**, um Echos zu vermeiden.
 * Stell sicher, dass du ein funktionierendes Headset hast. Du kannst in der Jitsi-Lobby überprüfen, ob dein Mikrofon funktioniert.
 * Achte darauf, dass keine lauten **Hintergrundgeräusche** entstehen (nimm zum Beispiel deine Kekse aus der raschelnden Plastikverpackung).
 * Wenn du kannst: Aktiviere die Geräuschunterdrückung an deinem Headset oder PC (zum Beispiel Noisetorch).
 * Du kannst uns bitten, dir in einem separaten Meeting beim **Testen** deines Setups zu helfen.

## Beim ersten Besuch:
  
 * Wenn eine neue Person im Meeting ist, gibt es eine **Vorstellungsrunde**, in der alle ein paar Worte über sich sagen können.
 * Wenn du zum ersten Mal dazukommst, **stell möglichst viele Fragen** zu F.U.C.K.. Die Fragen habe Priorität gegenüber anderen Themen.
 * Du kannst jetzt eine **Einladung in unseren Matrixraum** bekommen, in dem wir über alles Mögliche reden und Treffen koordinieren.
 
## Im Meeting:
  
 * Achte bitte darauf, **wie viel Raum du in der Diskussion einnimmst** (wie viel du im Vergleich zu anderen redest) und mach Platz für ruhigere Personen.
 * Unterbrich andere nicht.
 * Wenn du das Gefühl hast, dass du nicht zu Wort kommst, kannst du die "Hand heben"-Funktion verwenden.
 * Achte auf **Themen, die für Menschen emotional sensibel** sein könnten (zum Beispiel Drogen, Gewalt, psychische Gesundheit, Klimakatastrophe, usw.). **Frag im Zweifelsfall**, ob es für alle in Ordnung ist, darüber zu sprechen.
 * Du kannst jederzeit einen zweiten Raum eröffnen, wenn du über ein sensibles Thema oder ein Thema sprechen möchtest, das nur für Teile der Gruppe interessant ist (zum Beispiel Nischen-Tech-Talk).
 * Du kannst jederzeit darum bitten, einen zweiten Raum zu öffnen, wenn du das Gefühl hast, dass zu viele Personen im Raum sind (normalerweise machen wir das, wenn wir mehr als 6 Personen sind).
 * Natürlich: keine Diskriminierung, keine Screenshots, keine herablassenden Bemerkungen, keine vorgetäuschte Überraschung, usw.
 * Frag, ob es für alle in Ordnung ist, wenn du Alkohol trinkst (frag nochmal, wenn neue Leute kommen).
 * Sei nicht stark betrunken oder bekifft.
 * Wenn dich etwas stört, zum Beispiel ein Hintergrundgeräusch, blinkende Lichter, usw., ist die Wahrscheinlichkeit groß, dass du nicht die einzige Person bist, der davon gestört wird, und es ist völlig in Ordnung, die **Leute zu fragen, ob sie dafür sorgen können, dass es aufhört**.
 * **Achtet aufeinander.** Wenn du dein Eindruck hast, dass es einer Person gerade nicht gut geht, frag nach, ob alles OK ist.
 * Wenn du eine Situation beobachtest, die für eine andere Person unangenehm zu sein scheint, du jegliche Form von Diskriminierung oder Belästigung beobachtest oder eine Situation auf dich nur etwas seltsam wirkt, fühl dich dazu ermuntert dies anzusprechen. Eine Situation, die für dich nur etwas seltsam anmutet, kann für eine andere Person schon äußerst unangenehm sein.
