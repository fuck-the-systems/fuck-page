+++
title = "F.U.C.K. FAQ"
readmore = "Mehr Infos..."
translationKey = "faq"
slug = "faq"
+++

# Für wen ist F.U.C.K.?
Wir sind FLINTA* und mögen Computer Kram. Wenn das auch auf dich zutrifft, bist du hier richtig!


# Darf ich kommen, wenn ich noch gar nicht programmieren kann?
JA!!!


# Muss ich denn schon was können, um bei euch vorbeizuschauen?
Nein! Du musst weder programmieren können, noch ein Linux System am Laufen
haben oder mit der Kommandozeile vertraut sein. Wir wollen gemeinsam einen Raum schaffen,
in dem du dir das aneignen kannst, wenn du möchtest.


# Habt ihr einen Code of Conduct (CoC)?
Ja! Du findest unseren Code of Conduct [hier](/coc/). Wenn du Anmerkungen oder Vorschläge hast, kannst du die in unserem [gitlab Repo](https://gitlab.com/fuck-the-systems/code-of-conduct) anbringen oder uns direkt kontaktieren.

# Muss ich für ein online Treffen Zoom oder Google Meet oder sonst was installiert haben?
Nein. Die Treffen finden über Jitsi statt.
- am Computer du kannst den Link zum Meeting einfach im Browser öffnen, ohne was zu installieren (es kann sein, dass nicht alle Browser gleich gut funktionieren und wenn du Schwierigkeiten hast probier es vielleicht mit einem anderen Browser)


- am Handy:
 am Handy brauchst du die Jitsi App um dich verbinden zu können
 [fdroid](https://f-droid.org/de/packages/org.jitsi.meet/),
 [Android-Store](https://play.google.com/store/apps/details?id=org.jitsi.meet&gl=DE)
 [Apple-Store](https://apps.apple.com/de/app/jitsi-meet/id1165103905)


# Was passiert während der Meetings?
Schau bei den [Terminen](/termine/) nach und klicke auf mehr Informationen. Dort findest du eine Beschreibung zu den Terminen.
Wir haben einen [Guide für Meetings](/meetings/), den du dir durchlesen kannst.


# Wie bekomme ich den Link zu einem Treffen?
Den Link für die Veranstaltungen gibts im [fuck-matrix](https://matrix.to/#/#public:fuck-the.systems), per [email](mailto:hello@fuck-the.systems) oder  [Mastodon DM](https://chaos.social/@fuckthesystems)


# Ich bin schon dabei und würde gerne eine Person mitbringen
Du kannst den Link gerne an Einzelpersonen weiterleiten, bitte poste die Links zu den Veranstaltungen nirgends öffentlich.


# Ich würde gerne mitmachen, mag aber keine Video Calls
Es gibt auch einen Matrix Space, in dem du auch ganz ohne Video Calls bei F.U.C.K. mitmachen kannst.
Ping uns einfach über [fuck-matrix](https://matrix.to/#/#public:fuck-the.systems), per [email](mailto:hello@fuck-the.systems), [Twitter DM](https://twitter.com/_fuckthesystems) oder  [Mastodon DM](https://chaos.social/@fuckthesystems)


# Wie komme ich in den Matrix Space?
In den Matrix Space kannst du eingeladen werden, wenn du bei einem unserer online Treffen warst oder wenn du eine Person kennst, die schon drin ist. Wenn du schon einen Account hast, kannst du uns einfach deinen Account Namen sagen und bekommst eine Einladung.
Wenn du noch keinen hast, gibt es [hier](/matrix/) eine Anleitung, wie du dich anmelden kannst.

# Kann ich die F.U.C.K. Infrastruktur für andere feministische Projekte nutzen?
Ja, ihr könnt gerne unser Matrix, Jitsi oder Etherpads verwenden.
