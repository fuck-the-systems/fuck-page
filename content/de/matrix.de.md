+++
title = "[matrix] - How to"
hideReadMore = true
translationKey = "matrix"
slug = "matrix"
+++

# Was ist [matrix]?
[matrix] ist ein föderiertes Chatprotokoll so wie IRC oder XMPP (früher auch als Jabber bekannt). [element](https://element.io/get-started) ist der populärste Client für [matrix]. Du kannst auch andere Clients verwenden aber es kann dann sein, dass manches nicht so funktioniert wie für die meisten (zum Beispiel Ende zu Ende Verschlüsselung). [element](https://element.io/get-started) gibt es als Browser App, für den Desktop (Linux, Mac, Windows), Android und iPhone.

Wir haben unseren eigenen Server aber du musst keinen Account auf unserem Server haben, um in unseren Space zu kommen.

# Was ist ein Space?
Damit nicht unsere ganze Kommunikation in einem einzigen Raum stattfindet, haben wir einen F.U.C.K. Space in dem verschiedene Räume zu unterschiedlichen Themen gebündelt sind (das ist so wie Workspaces bei Slack). Wenn du in unseren Space kommst kannst du alle Räume sehen und wenn du möchtest auch beitreten.

# Wie kann ich mich anmelden?
Wenn du noch keinen Account auf unserem Server hast, kannst du von uns einen Anmeldungstoken bekommen. Mit dem Token, kannst du einen Account registrieren. Die folgenden Screenshots sind von der Browser App.

# Was mach ich mit dem Token?
Du hast von uns einen Link bekommen mit dem du dich anmelden kannst. Der Token ist da schon eingetragen oder du kannst ihn im token Feld eintragen. Such dir einen Username aus uns benutze ein sicheres Passwort.


![fuck-the.systems registration](/element/00_register.png)

# Einloggen
Du kommst dann zum Sign-in Screen, die folgenden Schritte kannst du auch in deiner bevorzugten App erledigen.


![fuck-the.systems Signin](/element/01_signin.png)

# Der richtige Home Server
Beim sign in form musst du darauf achten, dass der richtige Home Server eingetragen ist. Das ist vor allem dann wichtig, wenn du eine App benutzt in der nicht automatisch der richtige Home Server also ```fuck-the.systems``` gesetzt ist. Meistens steht da ```matrix.org```, der Standardserver von [matrix].


![fuck-the.systems Signin form](/element/02_signin_server.png)

# Einladung zum Space
Wenn du dich eingeloggt hast brauchst du noch die Einladung zum Space. Eine Einladung kannst du von jeder Person bekommen, die schon in unserem Space ist. Im Zweifelsfall kannst du uns aber auch in unserem Public Room schreiben. Oder du schreibst einfach über den selben Kommunikationskanal über den du auch bereits den Token erhalten hast. Sobald du eine Einladung bekommen hast erscheint auf der linken Seite ein Feld mit unserem Logo und einem roten Ausrufezeichen. Da klickst du drauf und solltest dann die Einladung annehmen können. Falls das nicht sofort klappt frag am besten eine*n von uns, damit wir das gemeinsam hinkriegen (manchmal ist das etwas langsam oder klappt nicht beim ersten Mal. Das Space feature ist noch nicht so alt und manchmal noch etwas unzuverlässig.)


![fuck Space invite](/element/04_invite_fuck.png)

# Die Raumübersicht
Als erstes solltest du nun die Raumübersicht von unserem Space sehen. Du kannst alle diese Räume joinen, für den Anfang sind vor allem ```main``` und ```welcome``` spannend. ```main``` ist unser zentraler Channel, um Infos zu teilen, zu plaudern, Events anzukündigen usw. Die anderen Räume sind zu bestimmten Themen.


![Übersicht der Räume](/element/06_room_overview.png)


Du kannst immer zur Raumübersicht zurück indem du auf das Space Logo auf der linken Seite klickst.


![Zurück zur Raumübersicht](/element/09_back_to_overview.png)


# Hurra du hast es geschafft! Happy chatting! <3
Alles was jetzt noch kommt ist optional bzw. wird vielleicht später für dich relevant.

# Secure Backup?
Ziemlich bald wird oben auf der linken Seite ein Dialog angezeigt, der dich fragt ob du einen Secure Backup machen magst. Den solltest du speichern, damit du in Zukunft auch verschlüsselte Nachrichten lesen kannst, wenn du dich ausgeloggt hattest oder ein anderes Device verwenden möchtest. Speicher den Code am besten in einem Passwortmanager ab oder an einem anderen Ort, wo du ihn wiederfindest aber am besten so, dass er (für alle, die nicht du sind) nicht zu einfach zu finden ist.


![Secure Backup](/element/07_secure_backup.png)


Element verschlüsselt Kommunikation Ende zu Ende (wenn das für den Raum aktiviert ist), das heißt du brauchst immer einen gültigen Schlüssel, um die Nachrichten in einem verschlüsselten Raum zu lesen. Falls du dich von einem neuen Device oder mit einem anderen Browser einloggst können die alten Nachrichten also nicht gelesen werden. Bei einem Login von einem neuen Device musst du also erst einmal beweisen, dass du berechtigt bist, die Nachrichten zu entschlüsseln. Es erscheint wieder oben links ein Dialog und dann hast du zwei Möglichkeiten.


![Verify Device](/element/10_verify_device.png)


Verify with Security Key bedeutet, dass du den Code, den du gespeichert hattest eingibst. Das funktioniert immer, egal ob du noch in einer anderen Session eingeloggt bist.

Alternativ kannst du deine Devices/Sessions gegenseitig verifizieren. Du musst dazu Zugang zu beiden Sessions gleichzeitig haben, also zum Beispiel Handy und Desktop. Wenn du mit einem Device eine Verifizierung startest kommt auf dem anderen gerät eine Verifizierungsanfrage.


![Verifizierungsanfrage](/element/13_other_device.png)


Du nimmst die Anfrage an und du vergleichst, ob bei beiden Devices die gleichen Emojis angezeigt werden. Wenn das so ist, musst du nur noch bestätigen und ab sofort sollten beide Devices in der Lage sein, die gleichen verschlüsselten Nachrichten zu lesen. Nach dem gleichen Prinzip funktioniert übrigens auch die Verifizierung mit anderen User*innen.

![Vergleiche Emojis](/element/15_emoji_compare.png)


# Probleme beim Einrichten?
Wir haben einen [Public Support Channel](https://element.fuck-the.systems/#/room/#support:fuck-the.systems) für schnelle Hilfe und Troubleshooting, wenn du schon auf Matrix bist. Ansonsten kannst du uns auf unseren anderen Kanälen anpingen, falls etwas nicht klappt.
