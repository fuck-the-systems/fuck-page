+++
title = "Feminismus und Computer Kram"
date = "2023-06-09"
cover = "logo-fuck-final.png"
translationKey = "home"
slug = "home"
+++

# Willkommen in unserem Online Hackspace

Wir sind FLINTA* und mögen Computer Kram. Wenn das auch auf dich zutrifft, bist du hier richtig! 


# Veranstaltungen
Alle Termine findes du unter [Termine](/termine/)


# Kontakt

Wir haben {{< rawhtml >}}<a rel="me" href="https://chaos.social/@fuckthesystems">Mastodon</a>{{< /rawhtml >}} 

unser public Matrix [Channel](https://matrix.to/#/#public:fuck-the.systems)

Schreib uns eine [Email](mailto:hello@fuck-the.systems)

# Projekte

Wir betreiben unsere eigene Infrastruktur: momentan ein Matrix Server, ein Jitsi Server und etherpads


*Frauen, Lesben, inter, nicht-binäre, trans und agender Personen