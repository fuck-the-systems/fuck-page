+++
title = "F.U.C.K Donnerstag"
summary = "Jeden 1., 3. und 5. Donnerstag im Monat ab 20:00 auf Deutsch"
readmore = "Weitere Infos..."
weight = 10
translationKey = "donnerstag"
slug = "fuck-donnerstag"
+++

# Wann?
Am 1., 3. und 5. Donnerstag im Monat (alternierend mit [F.U.C.K. Thursday](/termine/recurring/fuck-thursday/))  
20:00 bis niemand mehr da ist

# Wo?
Auf Jitsi (Link gibts im [F.U.C.K.-Matrix](https://matrix.to/#/#public:fuck-the.systems), per [Email](mailto:hello@fuck-the.systems) oder [Mastodon DM](https://chaos.social/@fuckthesystems))

# Was?
Das Donnerstags Meeting ist der beste Ort, um einfach mal vorbei zu schauen und F.U.C.K. kennen zu lernen. Es gibt Vorstellungsrunden und kurze Erklärungen zum F.U.C.K. Netzwerk. Das Ganze ist einfach eine nette Plauderrunde, wo die Themen von Woche zu Woche variieren, je nach dem worüber die Anwesenden gerade plaudern wollen. Es gibt auch die Möglichkeit kleinere Gruppen zu bilden, wenn es mehrere Themen gibt oder die Gruppe zu groß wird.

# Sprache: Deutsch

# Guide für unsere Meetings
Wenn du zum ersten mal bei einem unserer Meetings vorbei kommen möchtest, nimm dir bitte kurz Zeit unseren [Guide für Meetings](/meetings/) durchzulesen.