+++
title = "F.U.C.K. Code Puzzles"
summary = "Jeden Donnerstag von 19:00 - 20:00 uhr"
readmore = "Weitere Infos..."
weight = 40
translationKey = "code-puzzles"
slug = "fuck-code-puzzles"
+++

# Wann?
Jeden Donnerstag von 19:00 - 20:00 uhr (vor F.U.C.K. Thursday oder F.U.C.K. Donnerstag)

# Wo?
Auf Jitsi (link gibts im [F.U.C.K.-Matrix](https://matrix.to/#/#public:fuck-the.systems), per [Email](mailto:hello@fuck-the.systems) oder [Mastodon DM](https://chaos.social/@fuckthesystems)).

# Was?
Vor dem Donnerstags Meeting treffen wir uns und versuchen gemeinsam an einem Coding Puzzle zu tüfteln. Ihr könnt gerne Puzzles mitbringen, ansonsten wird es vermutlich ein Coding Puzzle aus dem Internet.
Wenn wir mehr als 3 Leute sind, werden wir uns in kleinere Gruppen aufteilen. Die Sprache mit der wir das Lösen ergibt sich aus den anwesenden in der Gruppe.
Auch wenn ihr noch gar nicht wisst wie anfangen, seid ihr herzlich willkommen und dürft immer gerne mittüfteln.

# Sprache
Deutsch oder Englisch je nachdem wer in der Gruppe ist.

# Guide für unsere Meetings
Wenn du zum ersten mal bei einem unserer Meetings vorbei kommen möchtest, nimm dir bitte kurz Zeit unseren [Guide für Meetings](/meetings/) durchzulesen.