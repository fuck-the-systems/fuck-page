+++
title = "F.U.C.K. Dienstag - Dinge tun"
summary = "Jeden 1. und 3. Dienstag im Monat ab 19:00"
readmore = "Weitere Infos..."
weight = 30
translationKey = "tuesday"
slug = "fuck-dienstag"
+++

# Wann?
Am 1. und 3. Dienstag im Monat um 19:00

# Wo?
Auf Jitsi (Link gibts im [F.U.C.K.-Matrix](https://matrix.to/#/#public:fuck-the.systems), per [Email](mailto:hello@fuck-the.systems) oder [Mastodon DM](https://chaos.social/@fuckthesystems))

# Was?
Die Dienstags Treffen sind dafür gedacht gemeinsam (oder alleine) an Projekten zu arbeiten. Um 19:00 gibt es einen kurzen Call, in dem wir uns erzählen, woran wir arbeiten. Danach teilen wir uns in kleinere Gruppen auf oder Leute verlassen das Jitsi wieder, um in Ruhe an ihrem Projekt zu arbeiten.
Ihr könnt schon fertige Projektideen mitbringen. Wenn ihr gerne Coden anfangen wollt, aber noch nicht so recht wisst wie und wo, könnt ihr auch vorbeikommen und euch Tipps und Links zu Tutorials holen.

# Sprache
Deutsch oder Englisch je nachdem wer in der Gruppe ist.

# Guide für unsere Meetings
Wenn du zum ersten mal bei einem unserer Meetings vorbei kommen möchtest, nimm dir bitte kurz Zeit unseren [Guide für Meetings](/meetings/) durchzulesen.